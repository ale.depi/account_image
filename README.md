# Account Image

[current logo](https://gitlab.com/ale-depi/account-image/-/jobs/artifacts/master/raw/releases/02.pdf?job=compile_pdf)

## Description

The aim of this project was to create an image for my accounts like instant
messaging, mail and so on.

I was impressed by the geometry and philosophy of my GitLab image, so I decided 
to derive my new image from the GitLab one. Sadly, many times, it cannot be
used as vectorial.

## History

* 2020 04 27  
  I have created the repo with the initial version of my account image. These
  were my first attempt of using Git and GitLab. In the repo, also other
  formats were added, for instance, svg, png and ps.

* 2020 09 17  
  I have changed colors and resized capitol A.

* Meanwhile I have discovered that the account image of GitLab is the identicon
  coming from [Gravatar](https://www.gravatar.com/) service. Moreover, GitLab
  has CI/CD!

* 2022 06 15  
  I have decided to rewrite the repo in order to clean it. I have rebased it
  keeping the meaningful commits and changes.

* 2023 07 03  
  Time to change the logo. I have reorganised the repo in order to show the
  evolution of both my project and the gravatar evolution.
